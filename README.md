# Pulumi AWS VPC
Scripts for creating AWS VPC infra with Public, Private, and Isolated EC2 instances using [pulumi api](https://www.pulumi.com).

## [Target infra diagram](https://lucid.app/lucidchart/3564703c-5c4d-4aa7-9717-eefb5b514663/view)
## [![Diagram](https://gitlab.com/IvanDimanov/pulumi-aws-vpc/-/raw/master/image.png)](https://lucid.app/lucidchart/3564703c-5c4d-4aa7-9717-eefb5b514663/view)

## Target infra components
After a successful deploy, you'll have this infrastructure:
| AWS component | Description |
| :-- | :-- |
| VPC | Main network where all components reside |
| Availability Zone A | VPC section within a subregion |
| Public Subnet 1 | Every instance in this network can access the Internet, can be accessed back from the Internet, and can communicate with other instances within the VPC. Perfect for Web servers. |
| Private Subnet 2 | Instances in this network can access the Internet but cannot be accessed back from the Internet. Instances can communicate with other instances within the VPC. Great for internal workers that may need to install updates/packages from the Internet but do not need the Internet to know about them |
| Isolated Subnet 3 | Instances in this network cannot access the Internet and cannot be accessed back from the Internet. They can only access other instances within the VPC |
| PublicSG | Allow traffic to and from Internet for a specific instance. Allow internal communication as well |
| PrivateSG | Allow traffic to Internet but not from Internet for a specific instance. Allow internal communication as well |
| IsolatedSG | Allow only internal communication |
| Route53 DNS Resolution | Binds VPC components to temporary or elastic IP addresses |
| Internet Gateway | Make access to Internet possible for the related VPC |
| NAT Gateway | Make access to Internet possible for the related VPC |
| Public Route table w/ Internet Gateway | Every connected subnet can access the Internet and can be accessed back |
| Public Route table w/ NAT Gateway | Every connected subnet can access the Internet but cannot be accessed back |
| Public Route table w/o NAT w/o IG | Connected subnets can access only internet VPC instances |


## Tech stack
- [pulumi api](https://www.pulumi.com)
- [TypeScript](https://www.typescriptlang.org)


## Run locally
Make sure you have [pulumi installed](https://www.pulumi.com/docs/get-started/aws/begin/#install-pulumi).
```
git clone git@gitlab.com:IvanDimanov/pulumi-aws-vpc.git
cd ./pulumi-aws-vpc
npm ci
cp .env.example .env
cd ./infra
pulumi up
```

Pulumi uses your AWS account to create the desired infrastructure
so once the scrip is completed
you can verify the result in your [AWS console](https://console.aws.amazon.com/ec2/v2/home?region=us-east-1#Instances:)
or SSH into your new Public EC2 instance.


## ENV VARs
You can change any of the default ENV VARs from `.env` file.
```
AWS_PREFIX= Give your instance some unique name, useful for debugging
AWS_SSH_KEY_NAME= Which SSH key you'd like to use when connecting to your Public EC2 instance
SSH_IP= Make sure the Public EC2 instance accepts SSH connection only from this IP address (most likely your public IP)
VPC_CIDR_BLOCK= What is the the CIDR you want your VPC to use, e.g. 10.0.0.0/16
EC2_INSTANCE_TYPE= How powerful you'd like your EC2 instances to be, e.g. t2.micro
```
